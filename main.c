//main.c
//gcc main.c -o main -I/usr/include/allegro5 -L/usr/lib -lm -lallegro -lallegro_acodec -lallegro_audio -lallegro_color -lallegro_dialog -lallegro_font -lallegro_image -lallegro_memfile -lallegro_physfs -lallegro_primitives -lallegro_ttf && astyle --indent=tab main.c

#include <stdio.h>
#include <math.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_color.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_memfile.h>
#include <allegro5/allegro_physfs.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>

#include "mersenne_t.h"
#include "util.h"

const float FPS = 60;

const int HOW_MANY_PARTICLES=100;
float particles[9][1000000];
int x=0;
int y=1;
int z=2;
int x_speed=3;
int y_speed=4;
int z_speed=5;
int r=6;
int g=7;
int b=8;

int ev_key_up(int which_key) {
	if (which_key == ALLEGRO_KEY_ESCAPE) doexit=true;
	return 0;
}

int ev_key_down(int which_key) {
	if (which_key == ALLEGRO_KEY_ESCAPE) doexit=true;
	return 0;
}

int main(int argc, char **argv) {

	for (int i = 1; i < HOW_MANY_PARTICLES; i++)
	{
		particles[x][i]=0.0;
		particles[y][i]=10.0;
		particles[z][i]=0.0;
		particles[x_speed][i]=0.0;
		particles[y_speed][i]=0.0;
		particles[z_speed][i]=0.0;
		particles[r][i]=0.0;
		particles[g][i]=0.0;
		particles[b][i]=0.0;
	}

#include "init.h"

	int counter = 0;
	while(!doexit)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		if(ev.type == ALLEGRO_EVENT_TIMER) {
			for (int i = 1; i < HOW_MANY_PARTICLES; i++)
			{
				particles[x][i]+=particles[x_speed][i];
				particles[y][i]+=particles[y_speed][i];
				particles[z][i]+=particles[z_speed][i];
				particles[z_speed][i]-=0.1;
			}
			for (int i = counter; i <= counter; i++)
			{
				particles[x][i]=0.0;
				particles[y][i]=10.0;
				particles[z][i]=0.0;
				float heading=random_float(0.0,2.0*3.141596);
				float magnitude=random_float(0.0,2.0*3.141596);
				particles[x_speed][i]=sin(heading)*magnitude;
				particles[y_speed][i]=cos(heading)*magnitude;
				int min=-10.0;
				int max=10.0;
				particles[z_speed][i]=random_float(1.0,20.0);
				particles[r][i]=random_float(0.0,1.0);
				particles[g][i]=random_float(0.0,1.0);
				particles[b][i]=random_float(0.0,1.0);
			}
			redraw = true;
		}
		else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			doexit=true;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_AXES ||
		        ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {

			mouse_x = ev.mouse.x;
			mouse_y = ev.mouse.y;
		}
		else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
			//doexit=true;
		}

#include "keyboard.h"

		if(redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;
			al_clear_to_color(al_map_rgb(0,0,0));
			for (int i=0; i<HOW_MANY_PARTICLES; i++) {
				if (particles[y][i]>0.1) {
					al_draw_filled_circle(
					    1280.0+(10.0*particles[x][i]/particles[y][i]),
					    720.0-(10.0*particles[z][i]/particles[y][i]),
					    10.0, al_map_rgb(rgb(particles[r][i]),rgb(particles[g][i]),rgb(particles[b][i])) );
				}
			}
			al_flip_display();
		}
		counter++;
		if (counter >= HOW_MANY_PARTICLES) counter = 0;
	}

	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);
	al_destroy_sample(sample);

	return 0;
}
