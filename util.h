//sudo apt install -y liballegro5-dev build-essential astyle
//gcc main.c -o main -I/usr/include/allegro5 -L/usr/lib -lm -lallegro -lallegro_acodec -lallegro_audio -lallegro_color -lallegro_dialog -lallegro_font -lallegro_image -lallegro_memfile -lallegro_physfs -lallegro_primitives -lallegro_ttf
//astyle --indent=tab main.c

//regex
//\nALLEGRO_([^\n]*)\n
//case ALLEGRO_\1:\nkey[\1]=false;\nbreak;\n\n

enum MYKEYS {
	KEY_A, KEY_B, KEY_C, KEY_D, KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L, KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T, KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z, KEY_0, KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_PAD_0, KEY_PAD_1, KEY_PAD_2, KEY_PAD_3, KEY_PAD_4, KEY_PAD_5, KEY_PAD_6, KEY_PAD_7, KEY_PAD_8, KEY_PAD_9, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5, KEY_F6, KEY_F7, KEY_F8, KEY_F9, KEY_F10, KEY_F11, KEY_F12, KEY_ESCAPE, KEY_TILDE, KEY_MINUS, KEY_EQUALS, KEY_BACKSPACE, KEY_TAB, KEY_OPENBRACE, KEY_CLOSEBRACE, KEY_ENTER, KEY_SEMICOLON, KEY_QUOTE, KEY_BACKSLASH, KEY_BACKSLASH2, KEY_COMMA, KEY_FULLSTOP, KEY_SLASH, KEY_SPACE, KEY_INSERT, KEY_DELETE, KEY_HOME, KEY_END, KEY_PGUP, KEY_PGDN, KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_PAD_SLASH, KEY_PAD_ASTERISK, KEY_PAD_MINUS, KEY_PAD_PLUS, KEY_PAD_DELETE, KEY_PAD_ENTER, KEY_PRINTSCREEN, KEY_PAUSE, KEY_ABNT_C1, KEY_YEN, KEY_KANA, KEY_CONVERT, KEY_NOCONVERT, KEY_AT, KEY_CIRCUMFLEX, KEY_COLON2, KEY_KANJI, KEY_LSHIFT, KEY_RSHIFT, KEY_LCTRL, KEY_RCTRL, KEY_ALT, KEY_ALTGR, KEY_LWIN, KEY_RWIN, KEY_MENU, KEY_SCROLLLOCK, KEY_NUMLOCK, KEY_CAPSLOCK, KEY_PAD_EQUALS, KEY_BACKQUOTE, KEY_SEMICOLON2, KEY_COMMAND
};

bool doexit = false;

float random_float(float low, float high)
{
	float n=genrand_real1()*(high-low)+low;
	if (n<low) n=low;
	if (n>high) n=high;
	return n;
}

float rgb(float input)
{
	float output=input*255.0;
	if (output > 255.0) output=255.0;
	if (output < 0.0) output=0.0;
	return output;
}
